var myWidgets = function() {
  return {
    showInfo: true
  };
};

myWidgets.$inject = [];
angular.module('appModule').service('myWidgets', myWidgets);