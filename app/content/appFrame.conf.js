var appFrameConf = function() {
  return {
    ctrlId: 'appFrame',
    switchers: [
      { _id: 'start' },
      { _id: 'main' }
    ]
  };
};

appFrameConf.$inject = [];
angular.module('appModule').service('appFrameConf', appFrameConf);