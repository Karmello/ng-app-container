angular.module('appModule').constant('URLS', {
  AWS3_UPLOADS_BUCKET_URL: 'https://s3.amazonaws.com/laf.useruploads/',
  AWS3_RESIZED_UPLOADS_BUCKET_URL: 'https://s3.amazonaws.com/laf.useruploadsresized/',
  itemImg: 'public/imgs/item.png'
});

angular.module('appModule').constant('NUMS', {
  reportMaxPhotos: 10,
  photoMaxSize: 1048576
});